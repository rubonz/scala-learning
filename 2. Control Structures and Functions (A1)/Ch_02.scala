

/**
 * Created by t_tarasenko on 24.03.2015.
 */
object Ch_02 {
  // 1
  def signum(n: Int) = {
    if (n > 1) 1
    else if (n < 0) -1
    else 0;

  }

  println(signum(3))
  println(signum(0))
  println(signum(-2))

  //2
  // The result of the last expression. Type the last expression.

  //3
  //y = 1
  //val x = y

  //4
  for (i <- 10 to(0, -1)) println(i)

  //5
  def countdown(n: Int) = {
    for (i <- n to(0, -1)) println(i)
  }

  countdown(6)
  println()

  //6
  var hello: Long = 1
  for (i <- "Hello") hello *= i
  println(hello)

  //7
  var hello1: Long = 1;
  "Hello".foreach(hello1 *= _)

  //or val hello1 = "Hello".foldLeft(1L)(_ * _)

  //8
  def product(s: String): Long = {
    var hello: Long = 1
    for (i <- "Hello") hello *= i
    hello
  }

  println(hello)

  //9
  def product1(s: String): Long = {
    if (s.length == 0) 1
    else s(0) * product1(s drop 1)
  }

  println(product1("Hello"))

  //10
  def recursion(x: Double, n: Double): Double= {
    if (n == 0 ) 1
    else if (n > 0) x * recursion(x, n - 1)
    else  if (n > 0 & n % 2 == 0) math.pow(recursion(x, n / 2), 2)
    else 1 / recursion(x, -n)

  }
  println("Test func "+ recursion(2,2))
  println(math.pow(2,2))
 
}
